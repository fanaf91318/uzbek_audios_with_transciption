from selenium import webdriver 
import time
from selenium.webdriver.common.by import By
import requests
import pandas as pd
import random
import os
from pathlib import Path
import pathlib
mypath = Path().absolute()
driver = webdriver.Chrome()
driver.maximize_window()
driver.get("https://muznavo.net/load/uzbek_mp3/1")
time.sleep(3)

def clean_it(text):
        
        text=text.replace(' ','')
        text=text.replace('+','')
        text=text.replace('\n','')
        text=text.replace('-','')
        
        return text
def clean_text(lyrics):

        return lyrics.lower().replace('\n','')
        
def get_single_music_data(music_link):
        try:
                driver.switch_to.new_window()
                time.sleep(1)
                driver.get(music_link)
                time.sleep(1)
                button = driver.find_element(By.XPATH,'//*[@id="full"]/div[1]/div[2]/div[3]/div[3]/span')
                button.click()
                transcript=driver.find_element(By.XPATH,'//div[@class="sect-content full-text clearfix hide-this olma slice"]').text
                speaker_id,music_name=driver.find_element(By.XPATH,"//div[@class='fheader fx-row']").text.split('-')
                gender=random.randint(0,1)
                speaker_id=clean_it(speaker_id)
                music_name=clean_it(music_name)
                utteranceID=f'{speaker_id}_{music_name}'
                full_path=f'{mypath}\{speaker_id}\{music_name}'
                download_link=driver.find_element(By.XPATH,"//a[@class='fbtn fdl anim']").get_attribute('href')
                pathlib.Path(f'./audios/{speaker_id}').mkdir(parents=True, exist_ok=True) 
                time.sleep(3)
                music_content=requests.get(download_link).content
                with open(f"./audios/{speaker_id}/{music_name}.mp3","wb") as f:
                    f.write(music_content)
                driver.close()
                time.sleep(1)
                driver.switch_to.window(driver.window_handles[0])
                time.sleep(3)
                return [speaker_id,gender,utteranceID,full_path,clean_text(transcript)]
    
        except:
                driver.close()
                time.sleep(1)
                driver.switch_to.window(driver.window_handles[0])
                return 0
    


spk2gender_dict={'speakerID':[],
                 'gender':[]}
wav_dict={'uterranceID':[],
        'full_path_to_audio_file':[]}
text_dict={'uterranceID':[],
        'text_transcription':[]}
utt2spk_dict={'uterranceID':[],
        'speakerID':[]}
i=1
while True:
        print(f'Page {i}')
        music_links=[]
        page_elements=driver.find_elements(By.XPATH,"//a[@class='track-desc fx-1 nowrap wajax']")
        for page_element in page_elements:
                music_links.append(page_element.get_attribute('href'))
        j=1
        for music_link in music_links:
            print(j,'<=>',music_link)
            data=get_single_music_data(music_link)
            if data!=0:
                spk2gender_dict['speakerID'].append(data[0]) 
                spk2gender_dict['gender'].append(data[1]) 
                wav_dict['uterranceID'].append(data[2])
                wav_dict['full_path_to_audio_file'].append(data[3])
                text_dict['uterranceID'].append(data[2])
                text_dict['text_transcription'].append(data[4])
                utt2spk_dict['uterranceID'].append(data[2])
                utt2spk_dict['speakerID'].append(data[0]) 
                
                time.sleep(1)   
                j+=1
        
        
        spk2gender_file=pd.DataFrame(spk2gender_dict)
        wav_file=pd.DataFrame(wav_dict)
        text_file=pd.DataFrame(text_dict)
        utt2spk_file=pd.DataFrame(utt2spk_dict)
        spk2gender_file.to_csv('spk2gender_file.csv')
        wav_file.to_csv('wav_file.csv')
        text_file.to_csv('text_file.csv')
        utt2spk_file.to_csv('utt2spk_file.csv')
                

        i+=1
        time.sleep(10)
        driver.find_elements(By.XPATH,'//a[@class="swchItem"]')[-1].click()
        time.sleep(1)
    